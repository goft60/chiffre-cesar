#sources.delahayeyourself.info
# Explorer > Dept.Info.2A > wordscounter
from tkinter import *
from tkinter.messagebox import *
import string

res = None

def createLabel(frame, val_text, num_row):
    ### createLabel
    ## Fonction retournant le label présent sur l'interface qui vient d'être créé
    ## 3 paramètres : frame => la frame tkinter dans laquelle il doit s'afficher
    ## val_text => Texte du Label | num_row => Numéro de ligne du label
    # Exemple: 
    # tk = Tk()
    # new_label = createLabel(tk, "Hello World", 0)
    label_method = Label(frame, text=val_text)
    label_method.grid(column=0, row=num_row)
    return label_method
def createNewFrame(frame_title):
    ### createNewFrame
    ## Fonction retournant une nouvelle frame de titre frame_title
    # Exemple :
    # new_frame = createNewFrame("Hello World !")
    new_frame = Tk()
    new_frame.title(frame_title)
    return new_frame

def createInput(frame, num_row):
    ### createInput
    ## Fonction retournant la zone de saisie de texte qui vient d'être créée
    ## 2 paramètres : frame => la frame tkinter dans laquelle elle doit s'afficher | num_row => numéro de ligne du Text
    # Exemple: 
    # tk = Tk()
    # new_input = createInput(tk, 0)
    input_text = Text(frame, wrap="word")
    input_text.grid(column=0, row=num_row)
    return input_text

def createButton(frame, text_area, string_crypt, string_chiff , num_decalage):
    ### createButton
    ## Fonction retournant le bouton d'envoi de la saisie qui vient d'être créé
    ## 3 paramètres : frame => la frame tkinter dans laquelle elle doit s'afficher
    ## text_area => la zone de texte à crypter/décrypter | string_crypt => décryptage ou cryptage
    # Exemple: 
    # tk = Tk()
    # new_input = createInput(tk, 1)
    # new_button = createButton(tk, new_input, "Crypter")
    button_valid = Button(frame, text="Lancer !", command= lambda:onUserPressButton(text_area.get("0.0", 'end-1c').strip(), string_crypt,string_chiff , num_decalage))
    button_valid.grid(column=0, row=5)
    return button_valid

def crypt_decrypt(chaine, to_crypt, num_decalage):
    ### crypt_decrypt
    ## Fonction retournant la chaîne en entrée après transformation (décryptage ou cryptage)
    ## 3 paramètres : chaine => string à manipuler | to_crypt => booléen vrai si on crypte
    ## num_decalage => nombre de décalage de caractères
    # Exemple :
    # str = "Yolo tro b1 la prog !"
    # print(crypt_decrypt(str, True, 8))  décalage de chaque caractère de 8   
    if not to_crypt:
        num_decalage = -num_decalage
    chaine_crypted = ''
    for char in chaine:
        if char != ' ' and char != '\n':
            chaine_crypted = chaine_crypted + chr(ord(char) + num_decalage)
        else:
            chaine_crypted += char
    return chaine_crypted

def circul_crypt_decrypt(chaine, to_crypt, num_decalage):
    tab_lower = string.ascii_lowercase
    tab_upper = string.ascii_uppercase
    if not to_crypt:
        num_decalage = -num_decalage
    chaine_crypted = ''
    for char in chaine:
        if tab_lower.__contains__(char):
            index = tab_lower.index(char)
            chaine_crypted += tab_lower[(index + num_decalage)%26]
        elif tab_upper.__contains__(char):
            index = tab_upper.index(char)
            chaine_crypted += tab_upper[(index + num_decalage) %26]
        else :
            chaine_crypted += char
    return chaine_crypted

def onUserPressButton(text, to_crypt,chiffrement , num_decalage):
    ### onUserPressButton
    ## Callback appelée lors de l'appui sur le bouton d'envoi
    ## 2 paramètres : text => Texte saisi dans la zone | to_crypt => booléen vrai si cryptage
    # Exemple:
    # button_valid = Button(frame, text="Go !", command= lambda:onUserPressButton("Coucou", True))
    try:
        num_decalage = int(num_decalage.get())
    except TclError:
        num_decalage = -1
    if(len(text) == 0):
        showerror("Erreur", "La chaîne est vide !")
    elif(num_decalage < 0):
        showerror("Erreur", "Le nombre de décalage est invalide !")
    else:
        global res # On reprend la fenêtre de résultat qui peut être ouverte
        try: # Essai de la détruire
            res.destroy()
        except AttributeError: #On ne fait rien s'il n'y en avait aucune
            res = None
        finally: #Et on crée la fenêtre de résultat
            if (chiffrement.get() == "Circulaire"):
                res = createResultWindow(circul_crypt_decrypt(text, to_crypt.get() == "Crypter", num_decalage))
            else :
                res = createResultWindow(crypt_decrypt(text, to_crypt.get() == "Crypter", num_decalage))

def createResultWindow(text):
    ### createResultWindow
    ## Fonction retournant la frame de résultat créée
    ## 1 paramètre : text => texte de résultat à afficher
    # Exemple :
    # res_frame = createResultWindow("Bla bla bla je suis crypté !")
    result_frame = createNewFrame("Résultat")
    lbl = createLabel(result_frame, "Here is your new string : ", 0)
    text_res = createInput(result_frame, 1)
    text_res.insert(END, text)
    return result_frame
  
rb_method = ['Crypter', 'Décrypter']
rb_chiffrement = ['Circulaire', 'Non-Circulaire']

main_frame = createNewFrame("Chiffrement de César")
v = StringVar()
w = StringVar()
num_decalage = IntVar()
label = createLabel(main_frame, "Méthode : ", 0)
frame_res = None

for rb_indice in range(2):
    rb_ch = Radiobutton(main_frame, variable = w, text=rb_chiffrement[rb_indice], value=rb_chiffrement[rb_indice])
    rb_ch.grid(column = 1, row=rb_indice+1)
    rb = Radiobutton(main_frame, variable= v, text=rb_method[rb_indice] , value=rb_method[rb_indice])
    rb.grid(column=0, row=rb_indice+1)
    if rb_indice == 0:
        rb.select()
        rb_ch.select()  

inputtext = createInput(main_frame, 4)
input_decal = Entry(main_frame, textvariable=num_decalage, width=5, justify='center')
input_decal.grid(row=3)

button_go = createButton(main_frame, inputtext, v, w, num_decalage)
main_frame.mainloop()