import unittest
from cesar import *

class TestsCesarFunctions(unittest.TestCase):

    def test_cryptage(self):
        self.assertEqual(crypt_decrypt('', True),'')
        self.assertEqual(crypt_decrypt('1', True),'9')
        self.assertEqual(crypt_decrypt('A', True),'I')
        self.assertEqual(crypt_decrypt('0aTze1', True),'8i\\\x82m9')
        self.assertEqual(crypt_decrypt('abc de1 01b', True),'ijk lm9 89j')

    def test_decryptage(self):
        self.assertEqual(crypt_decrypt('', False),'')
        self.assertEqual(crypt_decrypt('9', False),'1')
        self.assertEqual(crypt_decrypt('I', False), 'A')
        self.assertEqual(crypt_decrypt('8i\\\x82m9', False),'0aTze1')
        self.assertEqual(crypt_decrypt('ijk lm9 89j', False), 'abc de1 01b')
